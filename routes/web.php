<?php

use App\Http\Controllers\ClientController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);
Route::get('/', function () {
    return view('index');
});
Route::view('/clientes', 'cliente-excel')->middleware('auth')->name('cliente.excel');
Route::post('/clientes', 'ClientController@fileImport')->middleware('auth')->name('import.cliente');

Route::view('/cabinet', 'cabinet-excel')->middleware('auth')->name('cabinet.excel');
Route::post('/cabinet', 'CabinetController@fileImport')->middleware('auth')->name('import.cabinet');

Route::view('/placa', 'placa-excel')->middleware('auth')->name('placa.excel');
Route::post('/placa', 'PlacaController@fileImport')->middleware('auth')->name('import.placa');

Route::get('/tecnicos', 'TecnicoController@show')->middleware('auth')->name('tecnico.list');
Route::get('/tecnicos/nuevo', 'TecnicoController@create')->middleware('auth');
Route::get('/tecnicos/{id}', 'TecnicoController@showForm')->middleware('auth')->name('tecnico.form');
Route::post('/tecnicos/create/{id?}', 'TecnicoController@store')->middleware('auth')->name('tecnico.create');

Route::get('/cabinet/status', 'CabinetController@showStatus')->middleware('auth')->name('cabinet.status.list');
Route::get('/cabinet/status/nuevo', 'CabinetController@createStatus')->middleware('auth');
Route::get('/cabinet/status/{id}', 'CabinetController@showFormStatus')->middleware('auth')->name('cabinet.status.form');
Route::post('/cabinet/status/create/{id?}', 'CabinetController@storeStatus')->middleware('auth')->name('cabinet.status.create');

//Route::get('/reportes/formluario-tecnico/2011-2018', 'FormularioTecnicoController@show2018')->middleware('auth')->name('reporte.formulario.tecnico.2018');
//Route::get('/reportes/formluario-tecnico/2019', 'FormularioTecnicoController@show2019')->middleware('auth')->name('reporte.formulario.tecnico.2019');
//Route::get('/reportes/formluario-tecnico/2022/2011-2018', 'FormularioTecnicoController@show2022_2018')->middleware('auth')->name('reporte.formulario.tecnico.2022.2018');
Route::get('/reportes/formluario-tecnico/{idTipoFormulario}', 'FormularioTecnicoController@showByTipoFormulario')->middleware('auth')->name('reporte.formulario.tecnico.por.tipo.formulario');

Route::get('/report/formulario-tecnico/pdf/{idFormulario}', 'FormularioTecnicoController@exportPdf')->middleware('auth')->name('export.report.formulario-tecnico.pdf');
Route::get('/report/formulario-tecnico/excel/{idFormulario}', 'FormularioTecnicoController@exportExcel')->middleware('auth')->name('export.report.formulario-tecnico.excel');
