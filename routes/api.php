<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('v1/login', 'AuthMobileController@login')->name('mobile.login');
Route::get('v1/perfil/{identificacion}', 'AuthMobileController@perfil')->name('mobile.perfil');

Route::get('v1/clientes', function() {
    return \App\Client::all();
});
Route::get('v1/cabinets', function() {
    return \App\Cabinet::all();
});
Route::get('v1/placas', function() {
    return \App\Placa::select("placa","anio_importacion")->get();
});
Route::get('v1/cabinets/status', function() {
    return \App\CabinetStatu::all();
});

Route::get('v1/formularios', function() {
    return \App\Formulario::where("estado", "ACTIVO")->get();
});
Route::post('v1/formularios', 'ApiFormulariosController@guardarFormulario')->name('guardarFormulario');
Route::post('v1/firma/tecnico', 'ApiFormulariosController@guardarFirmaTecnico')->name('guardarFirmaTecnico');
