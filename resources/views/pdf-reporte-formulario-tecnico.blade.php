<?php
date_default_timezone_set('America/Guayaquil');
?>
<html>
    <head>
        <style>
            @page { margin: 0; font: sans-serif; }
            header { position: fixed; top: 0px; left: 0px; right: 0px; height: 50px; font-size: 10px; color: darkgray; padding: 8px 16px; }
            footer { position: fixed; bottom: 0px; left: 0px; right: 0px; height: 50px; font-size: 14px; font-weight: bold; color: darkgray; text-align: right; padding: 8px 16px; }
            body { margin: 90px 45px }

            .pagenum:before {
                content: counter(page);
            }
            .headerTable{
                border-bottom: 0px solid black;
                font-size: 14px;
                color: black;
                font-weight: bold;
                background-color: #DDE1FF;
                padding: 6px;
            }
            .titleColumn{
                padding-top: 4px;
                padding-bottom: 4px;
                font-size: 12px;
            }
            .dataColumn{
                font-size: 12px;
            }
            .bordeTableBottom{
                border-bottom: 1px solid black;
                border-collapse: collapse;
            }
            .bordeTable{
                border: 1px solid black;
                border-collapse: collapse;
            }
            .pageBreak {
                page-break-after: always;
            }
        </style>
    </head>
    <body>
        <header>
            <table style="width: 100%;">
                <tr>
                    <td width="50%">
                        Ecufroster - Reporte de formulario técnico<br>
                        Fecha de reporte {{ date('Y-m-d') }}<br>
                        Hora de reporte {{ date('H:i:s') }}
                    </td>
                    <td style="text-align: right;"><img src="{{ asset('assets/images/logo-inverse.png') }}" width="10%"></td>
                </tr>
            </table>
        </header>

        <footer>Página <span class="pagenum"></span></footer>

        <main>
            <h1 align="center" style="font-size: 20px; margin-top: -4px">Reporte de formulario técnico</h1>

            <table style="width: 100%; margin-top: -12px">
                <tr>
                    <td class="headerTable" colspan="2">
                        Información del formulario
                    </td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom" width="30%">Número de Requerimiento:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->numero_requerimiento }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Tipo de formulario:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->tipoFormulario->nombre }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Técnico asignado:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->tecnico_asignado }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Fecha de envío al servidor:</td>
                    <td class="dataColumn bordeTableBottom">{{ explode(" ", $formularioTecnico->created_at)[0] }} {{ explode(" ", $formularioTecnico->created_at)[1] }}</td>
                </tr>
                <tr>
                    <td class="titleColumn">Fecha de requerimiento:</td>
                    <td class="dataColumn">{{ $formularioTecnico->fecha_requerimiento }} {{ $formularioTecnico->hora_requerimiento }}</td>
                </tr>
            </table>



            <table style="width: 100%; margin-top: 16px">
                <tr>
                    <td class="headerTable" colspan="2">
                        Información Cliente/Usuario
                    </td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom" width="30%">Código SAP:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCliente->codigo_sap }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Nombre:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCliente->cliente }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Fecha y hora de visita:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->fecha_finalizado_app }} {{ $formularioTecnico->hora_finalizado_app }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Local:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCliente->local_nombre }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Dirección:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCliente->direccion }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Ciudad:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCliente->ciudad }}</td>
                </tr>
                @if($formularioTecnico->tipoFormulario->id == 1)
                    <tr>
                        <td class="titleColumn bordeTableBottom">Foráneo:</td>
                        <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCliente->foraneo }}</td>
                    </tr>
                @endif
                <tr>
                    <td class="titleColumn bordeTableBottom">Territorio:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCliente->territorio }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Vendedor:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCliente->vendedor }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Teléfono vendedor:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCliente->vendedor_telefono }}</td>
                </tr>
                <tr>
                    <td class="titleColumn">Geolocalización:</td>
                    <td class="dataColumn"><a target="_blank" href="https://www.google.com/maps?q={{ $formularioTecnico->formCliente->latitud }},{{ $formularioTecnico->formCliente->longitud }}">Ver Mapa</a></td>
                </tr>
            </table>



            <table style="width: 100%; margin-top: 16px">
                <tr>
                    <td class="headerTable" colspan="2">
                        Información básica del cabinet
                    </td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom" width="30%">Marca:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetBasica->marca }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Modelo:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetBasica->modelo }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Tipo:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetBasica->tipo_equipo }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Número canastillas:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetBasica->numero_canastillas }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Placa:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetBasica->placa }}</td>
                </tr>
                <tr>
                    <td class="titleColumn bordeTableBottom">Serie:</td>
                    <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetBasica->serie }}</td>
                </tr>
                @if($formularioTecnico->tipoFormulario->id != 3)
                    <tr>
                        <td class="titleColumn">Año de importación:</td>
                        <td class="dataColumn">{{ $formularioTecnico->formCabinetBasica->anio_importacion }}</td>
                    </tr>
                @endif
            </table>




            @isset($formularioTecnico->formCabinetTecnica)
                <table style="width: 100%; margin-top: 16px">
                    <tr>
                        <td class="headerTable" colspan="2">
                            Información técnica del cabinet
                        </td>
                    </tr>
                    <tr>
                        <td class="titleColumn bordeTableBottom" width="30%">Tipo de gas:</td>
                        <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetTecnica->tipo_gas }}</td>
                    </tr>
                    <tr>
                        <td class="titleColumn bordeTableBottom">Familia del tipo de gas:</td>
                        <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetTecnica->familia_tipo_gas }}</td>
                    </tr>
                    <tr>
                        <td class="titleColumn bordeTableBottom">Carga de gas:</td>
                        <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetTecnica->carga_gas }}</td>
                    </tr>
                    <tr>
                        <td class="titleColumn bordeTableBottom">Voltios:</td>
                        <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetTecnica->voltaje }}</td>
                    </tr>
                    <tr>
                        <td class="titleColumn bordeTableBottom">Amperios:</td>
                        <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetTecnica->consumo }}</td>
                    </tr>
                    @if($formularioTecnico->tipoFormulario->id == 1)
                        <tr>
                            <td class="titleColumn bordeTableBottom">KWH/24 horas:</td>
                            <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetTecnica->kwh_24_horas }}</td>
                        </tr>
                        <tr>
                            <td class="titleColumn bordeTableBottom">KWH/24 Mes aproximado:</td>
                            <td class="dataColumn bordeTableBottom">{{ $formularioTecnico->formCabinetTecnica->kwh_mes_aproximado }}</td>
                        </tr>
                        <tr>
                            <td class="titleColumn">Beneficios al medio ambiente:</td>
                            <td class="dataColumn">{{ $formularioTecnico->formCabinetTecnica->beneficios_medio_ambiente }}</td>
                        </tr>
                    @endif
                </table>
            @endisset



            <table style="width: 100%; margin-top: 16px">
                <tr>
                    <td class="headerTable" colspan="2">
                        Tipos de Status
                    </td>
                </tr>
                <tr>
                    <td class="titleColumn" width="30%">Tipos de status:</td>
                    <td class="dataColumn">{{ $formularioTecnico->formObservacionTecnica->status_cabinet }}</td>
                </tr>
                @if($formularioTecnico->tipoFormulario->id == 3)
                    <tr>
                        <td class="titleColumn">Tipo de movimiento:</td>
                        <td class="dataColumn">{{ $formularioTecnico->formObservacionTecnica->tipo_movimiento }}</td>
                    </tr>
                @endif
            </table>




            <table style="width: 100%; margin-top: 16px">
                <tr>
                    <td class="headerTable" colspan="2">
                        Observaciones
                    </td>
                </tr>
                <tr>
                    <td class="titleColumn" width="30%">Observación técnica:</td>
                    <td class="dataColumn">{{ $formularioTecnico->formObservacionTecnica->observacion_tecnica }}</td>
                </tr>
            </table>





            <!--<div class="pageBreak"></div>-->
            <br>
            <table style="width: 100%;">
                <tr>
                    <td class="headerTable" colspan="2">
                        Firmas
                    </td>
                </tr>
                <tr>
                    <td class="bordeTableBottom bordeTable" style="padding-left: 15px" width="30%">Firma Técnico:</td>
                    <td class="bordeTableBottom bordeTable" style="text-align:center;"><img src="{{ $formularioTecnico->tecnico->firma->imagen }}" width="10%"></td>

                </tr>
                <tr>
                    <td class="bordeTableBottom bordeTable" style="padding-left: 15px" width="30%">Firma Cliente:</td>
                    <td class="bordeTableBottom bordeTable" style="text-align:center;"><img src="{{ $formularioTecnico->formFirma->imagen }}" width="10%"></td>
                </tr>
            </table>




			<div class="pageBreak"></div>
			<table style="width: 100%;">
                <tr>
                    <td class="headerTable" colspan="3">
                        Registro de percha
                    </td>
                </tr>
				
				@foreach($formularioTecnico->fotosReporte as $item => $foto)
				<?php
				if(($item + 1) == 1 || ($item + 1) == 4){
				?>
				<tr style="margin-bottom: 6px;">
				<?php
				}
				?>
					<td>
						<img src="{{ $foto->imagen }}" width="33%">
					</td>
				<?php
				if(($item + 1) == 3){
				?>
				</tr>
				<?php
				}
				?>
				@endforeach
				
				@if(count($formularioTecnico->fotosReporte) > 0)
                    </tr>
                @endif
            </table>
        </main>
    </body>
</html>
