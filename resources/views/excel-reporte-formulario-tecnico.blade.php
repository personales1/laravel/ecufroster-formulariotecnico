<?php
date_default_timezone_set('America/Guayaquil');
?>
<div>
    <table>
        <tr>
            <td colspan="3">Ecufroster - Reporte de formulario técnico</td>
        </tr>
        <tr>
            <td colspan="3">Fecha de reporte {{ date('Y-m-d') }}</td>
        </tr>
        <tr>
            <td colspan="3">Hora de reporte {{ date('H:i:s') }}</td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="font-size: 28px; color: black; font-weight: bold" colspan="3">
                <h1 align="center">Reporte de formulario técnico</h1>
            </td>
        </tr>
    </table>


    <table style="width: 100%;">
        <tr>
            <td style="border-bottom: 0 solid black; font-size: 18px; color: black; font-weight: bold; background-color: #DDE1FF;" colspan="3">
                Información del formulario
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Número de Requerimiento</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->numero_requerimiento }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Tipo de formulario</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->tipoFormulario->nombre }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Técnico asignado</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->tecnico_asignado }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Fecha de requerimiento</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->fecha_requerimiento }} {{ $formularioTecnico->hora_requerimiento }}</td>
        </tr>
    </table>



    <table style="width: 100%; margin-top: 16px">
        <tr>
            <td style="border-bottom: 0 solid black; font-size: 18px; color: black; font-weight: bold; background-color: #DDE1FF;" colspan="3">
                Información Cliente/Usuario
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Código SAP</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->codigo_sap }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Nombre</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->cliente }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Fecha y hora de visita</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->fecha_finalizado_app }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Local</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->local_nombre }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Dirección</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->direccion }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Ciudad</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->ciudad }}</td>
        </tr>
        @if($formularioTecnico->tipoFormulario->id == 1)
            <tr>
                <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Foráneo:</td>
                <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->foraneo }}</td>
            </tr>
        @endif
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Territorio</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->territorio }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Vendedor</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->vendedor }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Teléfono vendedor</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->vendedor_telefono }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Geolocalización</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2"><a target="_blank" href="https://www.google.com/maps?q={{ $formularioTecnico->formCliente->latitud }},{{ $formularioTecnico->formCliente->longitud }}">Ver Mapa</a></td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Coordenadas</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCliente->latitud }},{{ $formularioTecnico->formCliente->longitud }}</td>
        </tr>
    </table>



    <table style="width: 100%; margin-top: 16px">
        <tr>
            <td style="border-bottom: 0 solid black; font-size: 18px; color: black; font-weight: bold; background-color: #DDE1FF;" colspan="3">
                Información básica del cabinet
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Marca</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetBasica->marca }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Modelo</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetBasica->modelo }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Tipo</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetBasica->tipo_equipo }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Número canastillas</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetBasica->numero_canastillas }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Placa</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetBasica->placa }}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Serie</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetBasica->serie }}</td>
        </tr>
        @if($formularioTecnico->tipoFormulario->id != 3)
            <tr>
                <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Año de importación</td>
                <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetBasica->anio_importacion }}</td>
            </tr>
        @endif
    </table>




    @isset($formularioTecnico->formCabinetTecnica)
        <table style="width: 100%; margin-top: 16px">
            <tr>
                <td style="border-bottom: 0 solid black; font-size: 18px; color: black; font-weight: bold; background-color: #DDE1FF;" colspan="3">
                    Información técnica del cabinet
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Tipo de gas</td>
                <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetTecnica->tipo_gas }}</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Familia del tipo de gas</td>
                <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetTecnica->familia_tipo_gas }}</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Carga de gas</td>
                <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetTecnica->carga_gas }}</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Voltios</td>
                <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetTecnica->voltaje }}</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Amperios</td>
                <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetTecnica->consumo }}</td>
            </tr>
            @if($formularioTecnico->tipoFormulario->id == 1)
                <tr>
                    <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">KWH/24 horas</td>
                    <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetTecnica->kwh_24_horas }}</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">KWH/24 Mes aproximado</td>
                    <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetTecnica->kwh_mes_aproximado }}</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Beneficios al medio ambiente</td>
                    <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formCabinetTecnica->beneficios_medio_ambiente }}</td>
                </tr>
            @endif
        </table>
    @endisset



    <table style="width: 100%; margin-top: 16px">
        <tr>
            <td style="border-bottom: 0 solid black; font-size: 18px; color: black; font-weight: bold; background-color: #DDE1FF;" colspan="3">
                Observaciones
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; border-collapse: collapse; font-weight: bold" colspan="1">Observación técnica</td>
            <td style="border-bottom: 1px solid black; border-collapse: collapse;" colspan="2">{{ $formularioTecnico->formObservacionTecnica->observacion_tecnica }}</td>
        </tr>
    </table>
</div>
