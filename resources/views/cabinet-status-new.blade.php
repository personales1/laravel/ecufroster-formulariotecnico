@extends('app')
@section('title', 'Cabinet status')

<?php
$_GET['page'] = "ubicacion";
$_GET['page-title'] = "Cabinet status";
if(isset($status)){
    $_GET['page-description'] = "Actualizar técnico ". $status->nombre;

}else{
    $_GET['page-description'] = "Registro de nuevos técnicos.";
}
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de status de cabinet</h5>
            <form id="myform" method="POST" action="{{ route('cabinet.status.create', ['id' => $status->id ?? '']) }}" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-5 mb-3">
                        <label for="tecnicoName">Nombre</label>
                        <input name="name" id="tecnicoName" type="text" class="form-control" placeholder="Nombre" value="{{ $status->nombre ?? old('name') }}" required>
                        @error('name')
                        <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                        <script>
                            $("#tecnicoName").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#tecnicoName").on("keyup", function (e) {
                let cadena = $("#tecnicoName").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#tecnicoName").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
