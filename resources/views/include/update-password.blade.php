<div class="app-container">
    <div class="h-100 bg-animation">
        <div class="d-flex h-100 justify-content-center align-items-center">
            <div class="mx-auto app-login-box col-md-8">
                <div class="modal-dialog w-100 mx-auto">
                    <div class="modal-content">
                        <form>
                            @csrf
                            <div class="modal-body">
                                <div class="h5 modal-title text-center">
                                    <h4 class="mt-2">
                                        <div>ECUFROSTER</div>
                                        <span>Por favor, actualiza tu contraseña para empezar a trabajar en el sistema de ECUFROSTER.</span>
                                    </h4>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="position-relative form-group">
                                            <label id="labelCedula" for="userCedula" class="">Cédula</label>
                                            <input name="cedula" id="userCedula" placeholder="Tu número de cédula aquí" type="text" class="form-control" value="{{ old('cedula') }}" required>
                                            <label class="mensaje-error" id="mensaje-error-cedula" style="display: none;"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="position-relative form-group">
                                            <label id="labelPassword" for="userPassword" class="">Nueva contraseña</label>
                                            <input name="password" id="userPassword" placeholder="Tu contraseña aquí" type="password" class="form-control" required>
                                            <label class="mensaje-error" id="mensaje-error-password" style="display: none;"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="position-relative form-group">
                                            <label id="labelConfirmPassword" for="userConfirmPassword" class="">Confirmar contraseña</label>
                                            <input name="confirmPassword" id="userConfirmPassword" placeholder="Tu contraseña aquí" type="password" class="form-control" required>
                                            <label class="mensaje-error" id="mensaje-error-confirmpassword" style="display: none;"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <label class="mensaje-error fsize-1" id="mensaje-error-general" style="display: none"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer clearfix">
                                <div class="mx-auto">
                                    <button type="button" class="btn btn-primary btn-lg btnChangePassword">Cambiar contraseña</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $("#userCedula").on("keyup", function (e) {
            let cadena = $("#userCedula").val();

            if(cadena.length > 0){
                $('#mensaje-error-cedula').hide();
                $("#userCedula").removeClass("form-control-error");
            }
        });

        $("#userPassword").on("keyup", function (e) {
            let cadena = $("#userPassword").val();

            if(cadena.length > 0){
                $('#mensaje-error-password').hide();
                $("#userPassword").removeClass("form-control-error");

                if(cadena === $("#userConfirmPassword").val().trim()){
                    $('#mensaje-error-confirmpassword').hide();
                    $("#userConfirmPassword").removeClass("form-control-error");
                }else{
                    if($("#userConfirmPassword").val().trim().length > 0){
                        if(cadena != $("#userConfirmPassword").val().trim()){
                            $('#mensaje-error-confirmpassword').css({"display":"flex"});
                            $('#mensaje-error-confirmpassword').html("Las contraseñas ingresadas no coinciden.");
                            $("#userConfirmPassword").addClass("form-control-error");
                        }
                    }
                }
            }
        });

        $("#userConfirmPassword").on("keyup", function (e) {
            let cadena = $("#userConfirmPassword").val();

            if(cadena.length > 0){
                if(cadena === $("#userPassword").val().trim()){
                    $('#mensaje-error-confirmpassword').hide();
                    $("#userConfirmPassword").removeClass("form-control-error");
                }else{
                    $('#mensaje-error-confirmpassword').css({"display":"flex"});
                    $('#mensaje-error-confirmpassword').html("Las contraseñas ingresadas no coinciden.");
                    $("#userConfirmPassword").addClass("form-control-error");
                }
            }
        });

        $(".btnChangePassword").on("click", function (e) {
            if(formValido()){
                $('#lightbox-loader').css({"visibility":"visible"});

                $.ajax({
                    url:"{{ route('usuario.update.password') }}",
                    type: 'POST',
                    data: {
                        _token: $("input[name='_token']").val(),
                        identificacion: $("#userCedula").val().trim(),
                        password: $("#userPassword").val().trim(),
                        confirmPassword: $("#userConfirmPassword").val().trim()
                    },
                    success: function(data) {
                        $('#lightbox-loader').css({"visibility":"hidden"});

                        let myJsonResponse = $.parseJSON(data); //convierte el response string en json

                        if(myJsonResponse.estado === 200){
                            $('#mensaje-error-general').hide();
                            $('#mensaje-error-general').html("");
                            location.href = '/'
                        }else{
                            if(myJsonResponse.estado === 401){
                                $('#mensaje-error-general').css({"display":"flex"});
                                $('#mensaje-error-general').html(myJsonResponse.data.mensaje);
                            }else{
                                $('#mensaje-error-general').css({"display":"flex"});
                                $('#mensaje-error-general').html(myJsonResponse.data.mensaje);
                            }
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        alert("Hubo un error.");
                        $('#lightbox-loader').css({"visibility":"hidden"});
                    }
                });
            }
        });

        function formValido(){
            var retorna = true;

            if($("#userCedula").val().trim() === ""){
                $('#mensaje-error-cedula').css({"display":"flex"});
                $('#mensaje-error-cedula').html("Campo obligatorio");
                $("#userCedula").addClass("form-control-error");
                retorna = false;
            }

            if($("#userPassword").val().trim() === ""){
                $('#mensaje-error-password').css({"display":"flex"});
                $('#mensaje-error-password').html("Campo obligatorio");
                $("#userPassword").addClass("form-control-error");
                retorna = false;
            }

            if($("#userPassword").val().trim() != $("#userConfirmPassword").val().trim()){
                $('#mensaje-error-confirmpassword').css({"display":"flex"});
                $('#mensaje-error-confirmpassword').html("Las contraseñas ingresadas no coinciden.");
                $("#userConfirmPassword").addClass("form-control-error");
                retorna = false;
            }

            return retorna;
        }
    });
</script>
