<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <?php
                if($_GET['page'] == "cliente"){
                ?>
                    <i class="pe-7s-user icon-gradient bg-happy-itmeo"></i>
                <?php
                }
                else if($_GET['page'] == "credito"){
                ?>
                    <i class="pe-7s-credit icon-gradient bg-happy-itmeo"></i>
                <?php
                }
                else if($_GET['page'] == "ubicacion"){
                ?>
                    <i class="pe-7s-map-2 icon-gradient bg-happy-itmeo"></i>
                <?php
                }
                else if($_GET['page'] == "cobros"){
                ?>
                    <i class="pe-7s-cash icon-gradient bg-happy-itmeo"></i>
                <?php
                }
                else if($_GET['page'] == "reporte"){
                    ?>
                    <i class="pe-7s-note2 icon-gradient bg-happy-itmeo"></i>
                    <?php
                }
                else if($_GET['page'] == "graficos"){
                    ?>
                    <i class="pe-7s-graph icon-gradient bg-happy-itmeo"></i>
                    <?php
                }
                else if($_GET['page'] == "usuario_list"){
                    ?>
                    <i class="pe-7s-users icon-gradient bg-happy-itmeo"></i>
                    <?php
                }
                else if($_GET['page'] == "usuario"){
                    ?>
                    <i class="pe-7s-user icon-gradient bg-happy-itmeo"></i>
                    <?php
                }
                else{
                ?>
                    <i class="pe-7s-credit icon-gradient bg-happy-itmeo"></i>
                <?php
                }
                ?>
            </div>

            <div>
                <?php echo $_GET['page-title']; ?>
                <div class="page-title-subheading"><?php echo $_GET['page-description']; ?></div>
            </div>
        </div>                
    </div>
</div>
