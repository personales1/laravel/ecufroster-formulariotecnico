<div class="app-container">
    <div class="h-100 bg-plum-plate bg-animation">
        <div class="d-flex h-100 justify-content-center align-items-center">
            <div class="mx-auto app-login-box col-md-8">
                <div class="modal-dialog w-100 mx-auto">
                    <div class="modal-content">
                        <form id="myform" method="POST" action="{{ route('login') }}"> <!--  class="needs-validation was-validated" -->
                            @csrf
                            <div class="modal-body">
                                <div class="app-logo mx-auto mb-3"></div>
                                <div class="h5 modal-title text-center">
                                    <h4 class="mt-2">
                                        <div>Bienvenido a ECUFROSTER</div>
                                        <span>Por favor, inicia sesión con tu cuenta.</span>
                                    </h4>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="position-relative form-group">
                                            <label id="labelUser" for="userEmail" class="">Email</label>
                                            <input name="email" id="userEmail" placeholder="Email aquí..." type="email" class="form-control" value="{{ old('email') }}" required>
                                            @error('email')
                                                <label class="mensaje-error" id="mensaje-error-user">{{ $message }}</label>
                                                <script>
                                                    $("#userEmail").addClass("form-control-error");
                                                </script>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="position-relative form-group">
                                            <label id="labelPassword" for="userPassword" class="">Password</label>
                                            <input name="password" id="userPassword" placeholder="Password aquí..." type="password" class="form-control" required>
                                            @error('password')
                                                <label class="mensaje-error" id="mensaje-error-password">{{ $message }}</label>
                                                <script>
                                                    $("#userPassword").addClass("form-control-error");
                                                </script>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <!--
                                <div class="position-relative form-check"><input name="check" id="exampleCheck" type="checkbox" class="form-check-input"><label for="exampleCheck" class="form-check-label">Keep me logged in</label></div>
                                -->
                                <!--
                                <div class="divider"></div>
                                <h6 class="mb-0">No account? <a href="javascript:void(0);" class="text-primary">Sign up now</a></h6>
                                -->
                            </div>
                            <div class="modal-footer clearfix">
                                <!--
                                <div class="float-left"><a href="javascript:void(0);" class="btn-lg btn btn-link">Recover Password</a></div>
                                -->
                                <div class="mx-auto">
                                    <button type="submit" class="btn btn-primary btn-lg">Iniciar sesión</button>
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
                <!--
                <div class="text-center text-white opacity-8 mt-3">Copyright © ArchitectUI 2019</div>
                -->
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $("#userEmail").on("keyup", function (e) {
            let cadena = $("#userEmail").val();

            if(cadena.length > 0){
                $('#mensaje-error-user').hide();
                $("#userEmail").removeClass("form-control-error");
            }
        });

        $("#userPassword").on("keyup", function (e) {
            let cadena = $("#userPassword").val();

            if(cadena.length > 0){
                $('#mensaje-error-password').hide();
                $("#userPassword").removeClass("form-control-error");
            }
        });
    });
</script>
