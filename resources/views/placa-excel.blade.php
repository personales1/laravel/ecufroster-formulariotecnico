@extends('app')
@section('title', 'Placas')

<?php
$_GET['page'] = "placa";
$_GET['page-title'] = "Placas";
$_GET['page-description'] = "Registro de placas a través de un archivo de excel.";
?>

@section('content-body')
    @if(session('exito'))
        <div class="alert alert-success mt-3" role="alert">
            {{ session('exito') }}
        </div>
    @endif

    @error('file')
    <div class="alert alert-danger mt-3" role="alert">
        El archivo excel es requerido
    </div>
    @enderror

    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Importación de placas</h5>
            <form id="myform" method="POST" action="/placa" enctype="multipart/form-data">
            @csrf
            <!-- application/vnd.ms-excel: archivos de Excel 97-2003 (.xls) -->
                <!-- application/vnd.openxmlformats-officedocument.spreadsheetml.sheet: archivos de Excel 2007+ (.xlsx) -->
                <input class="btn w-100" name="file" type="file" accept="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
                <button class="btn btn-primary btnGuardar mt-3" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });
        });
    </script>
@endsection
