@extends('app')
@section('title', 'Técnicos')

<?php
$_GET['page'] = "ubicacion";
$_GET['page-title'] = "Técnicos";
$_GET['page-description'] = "Listado de técnicos.";
?>

@section('content-body')
    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
        <li class="nav-item">
            <a class="nav-link active" href="{{ Request::root() }}/tecnicos/nuevo">
                <span>Nuevo</span>
            </a>
        </li>
    </ul>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <h5 class="card-title">Listado de técnicos</h5>
                    <form class="needs-validation" novalidate>
                        @csrf
                        @include('include.buscador')

                        <table class="mb-0 table table-bordered my-table">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Identificación</th>
                                <th>Teléfono</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="myTable">
                            @foreach($tecnicos as $tecnico)
                                <tr>
                                    <td>{{ $tecnico->nombre }}</td>
                                    <td>{{ $tecnico->identificacion }}</td>
                                    <td>{{ $tecnico->telefono }}</td>
                                    <td>
                                        <button class="btn-shadow btn btn-primary btnActualizar" type="button" data-toggle="tooltip" data-placement="bottom" data-id="{{ $tecnico->id }}">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#input-busqueda").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $(".btnActualizar").on("click", function (e) {
                location.href = '/tecnicos/' + $(this).data("id");
            });
        })
    </script>
@endsection
