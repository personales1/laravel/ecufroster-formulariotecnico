@extends('app')
@section('title', 'Reporte')

<?php
$_GET['page'] = "ubicacion";
$_GET['page-title'] = "Reporte formulario técnico";
$_GET['page-description'] = "Reporte de formularios técnico creados desde la app móvil.";
?>

@section('content-body')
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <h5 class="card-title">Listado de formularios técnicos</h5>
                    <form class="needs-validation" novalidate>
                        @csrf
                        @include('include.buscador')

                        <h5><b>Total: {{ $formularioTecnico->total() }} reportes</b></h5>

                        <table class="mb-0 table table-bordered my-table">
                            <thead>
                            <tr>
                                <th>Requerimiento</th>
                                <th>Formulario</th>
                                <th>Técnico</th>
                                <th>Fecha finalizado</th>
                                <th>Cliente</th>
                                <th>PDF - EXCEL</th>
                            </tr>
                            </thead>
                            <tbody id="myTable">
                            @foreach($formularioTecnico as $formTecnico)
                                <tr>
                                    <td>{{ $formTecnico->numero_requerimiento }}</td>
                                    <td>{{ $formTecnico->tipoFormulario->nombre }}</td>
                                    <td>{{ $formTecnico->tecnico_asignado }}</td>
                                    <td>{{ $formTecnico->fecha_finalizado_app }} {{ $formTecnico->hora_finalizado_app }}</td>
                                    <td>{{ $formTecnico->formCliente->cliente }}</td>
                                    <td>
                                        <a class="btn-shadow btn btn-danger" href="{{ route("export.report.formulario-tecnico.pdf", ['idFormulario' => $formTecnico->id ]) }}" target="_blank"><i class="fa fa-file-pdf"></i></a>
                                        <a class="btn-shadow btn btn-success" href="{{ route("export.report.formulario-tecnico.excel", ['idFormulario' => $formTecnico->id ]) }}"><i class="fa fa-file-excel"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div>
                            {!! $formularioTecnico->links() !!}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#input-busqueda").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        })
    </script>
@endsection
