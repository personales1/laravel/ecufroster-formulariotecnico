@extends('app')
@section('title', 'Técnico')

<?php
$_GET['page'] = "ubicacion";
$_GET['page-title'] = "Técnico";
if(isset($tecnico)){
    $_GET['page-description'] = "Actualizar técnico ". $tecnico->nombre;

}else{
    $_GET['page-description'] = "Registro de nuevos técnicos.";
}
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de técnicos</h5>
            <form id="myform" method="POST" action="{{ route('tecnico.create', ['id' => $tecnico->id ?? '']) }}" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label for="tecnicoName">* Nombre</label>
                        <input name="name" id="tecnicoName" type="text" class="form-control" placeholder="Nombre" value="{{ $tecnico->nombre ?? old('name') }}" required>
                        @error('name')
                        <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                        <script>
                            $("#tecnicoName").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="tecnicoIdentificacion">* Identificación</label>
                        <input name="identificacion" id="tecnicoIdentificacion" type="text" class="form-control" placeholder="Identificación" value="{{ $tecnico->identificacion ?? old('identificacion') }}" required>
                        @error('identificacion')
                        <label class="mensaje-error" id="mensaje-error-identificacion">{{ $message }}</label>
                        <script>
                            $("#tecnicoIdentificacion").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="tecnicoTelefono">Teléfono</label>
                        <input name="telefono" id="tecnicoTelefono" type="text" class="form-control" placeholder="Teléfono" value="{{ $tecnico->telefono ?? old('telefono') }}">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="tecnicoCorreo">Correo electrónico</label>
                        <input name="correo" id="tecnicoCorreo" type="text" class="form-control" placeholder="Correo electrónico" value="{{ $tecnico->correo ?? old('correo') }}">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="tecnicoPassword">* Contraseña</label>
                        <input name="password" id="tecnicoPassword" type="password" class="form-control" placeholder="Contraseña" value="{{ $tecnico->password ?? old('password') }}" required>
                        @error('password')
                        <label class="mensaje-error" id="mensaje-error-password">{{ $message }}</label>
                        <script>
                            $("#tecnicoPassword").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#tecnicoName").on("keyup", function (e) {
                let cadena = $("#tecnicoName").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#tecnicoName").removeClass("form-control-error");
                }
            });

            $("#tecnicoIdentificacion").on("keyup", function (e) {
                let cadena = $("#tecnicoIdentificacion").val();

                if(cadena.length > 0){
                    $('#mensaje-error-identificacion').hide();
                    $("#tecnicoIdentificacion").removeClass("form-control-error");
                }
            });

            $("#tecnicoPassword").on("keyup", function (e) {
                let cadena = $("#tecnicoPassword").val();

                if(cadena.length > 0){
                    $('#mensaje-error-password').hide();
                    $("#tecnicoPassword").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
