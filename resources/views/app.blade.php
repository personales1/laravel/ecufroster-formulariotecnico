<!doctype html>
<html lang="en">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('include.meta')
    <title>ECUFROSTER @yield('title')</title>
    <link  href="{{asset('style.css?v=0')}}" rel="stylesheet">
    <link  href="{{asset('mystylecustom.css?v=0')}}" rel="stylesheet">

    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('assets/scripts/jquery-3.4.1.js') }}"></script>

    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{asset('assets/datePicker/css/bootstrap-datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('assets/datePicker/css/bootstrap-standalone.css')}}">
    <script src="{{asset('assets/datePicker/js/bootstrap-datepicker.js')}}"></script>

    <!-- Languaje -->
    <script src="{{asset('assets/datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

    <!-- SweetAlert2 -->
    <script type="text/javascript" src="{{ asset('assets/scripts/sweetalert2.all.min.js') }}"></script>
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    @auth
        @include('include.header')

        <div class="app-main">
            @include('include.menu')

            <div class="app-main__outer">
                <div class="app-main__inner">
                    @include('include.header-content-page')

                    @section('content-body')
                    @show
                </div>
            </div>
        </div>
    @else
        @include('include.login-body')
    @endauth
</div>

{{-- Loading que se usa en todas las pantallas --}}
<div id="lightbox-loader">
    <div>
        <img src="{{asset('assets/images/loading.gif')}}" width="250">
        <p>Procesando .....</p>
    </div>
</div>

<!-- Mostrar mensaje de alerta de éxito -->
<div class="toast-top-right toast-custom-success" id="toast-container" style="display: none">
    <div class="toast toast-success" aria-live="polite">
        <!--<button type="button" class="toast-close-button" role="button">×</button>-->
        <div class="toast-title" id="title-success"></div>
        <div class="toast-message" id="mensaje-success"></div>
    </div>
</div>

{{-- Se inserta un toast de error personalizado a través de la función createToastError() del archivo myjscustom.js --}}
<div class="mostrarToast"></div>

<script type="text/javascript" src="{{ asset('assets/scripts/myjscustom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/scripts/main.js') }}"></script>

<!-- Tiene que ir después del llamado de los js para que funcione el llamado al método showToastCustom()-->
<?php $miMensaje = session("success"); ?>
@isset($miMensaje)
    <script> showToastCustom("Mensaje", @json($miMensaje)); </script>
@endisset
</body>
</html>
