<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tecnico extends Model
{
    protected $table = "tecnicos";

    public function firma(){
        return $this->hasOne(FormFirmasTecnico::class);
    }

    public function formulario()
    {
        return $this->hasMany(FormInformacionFormulario::class);
    }
}
