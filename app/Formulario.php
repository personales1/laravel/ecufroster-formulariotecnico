<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formulario extends Model
{
    protected $table = "formularios";

    public function formulario()
    {
        return $this->hasMany(FormInformacionFormulario::class);
    }
}
