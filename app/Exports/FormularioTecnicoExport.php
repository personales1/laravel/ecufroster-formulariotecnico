<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class FormularioTecnicoExport implements FromView, WithColumnWidths
{
    protected $param;

    public function __construct($parametro)
    {
        $this->param = $parametro;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('excel-reporte-formulario-tecnico', [
            'formularioTecnico' => $this->param
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 30,
            'B' => 40,
            'C' => 40,
        ];
    }
}
