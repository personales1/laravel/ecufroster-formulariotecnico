<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "roles";

    protected $primaryKey = 'rol_id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function user(){
        return $this->hasMany(User::class);
    }
}
