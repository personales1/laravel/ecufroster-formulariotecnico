<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormInformacionFormulario extends Model
{
    protected $table = "form_informacion_formularios";

    public function formCliente(){
        return $this->hasOne(FormInformacionCliente::class,'form_id');
    }

    public function formCabinetBasica(){
        return $this->hasOne(FormInformacionCabinetBasica::class,'form_id');
    }

    public function formCabinetTecnica(){
        return $this->hasOne(FormInformacionCabinetTecnica::class,'form_id');
    }

    public function formObservacionTecnica(){
        return $this->hasOne(FormObservacionTecnica::class,'form_id');
    }

    public function formFirma(){
        return $this->hasOne(FormFirma::class,'form_id');
    }

    public function tecnico()
    {
        return $this->belongsTo(Tecnico::class);
    }

    public function fotosReporte()
    {
        return $this->hasMany(FormReporteFoto::class, 'form_id');
    }

    public function tipoFormulario()
    {
        return $this->belongsTo(Formulario::class, "id_tipo_formulario");
    }
}
