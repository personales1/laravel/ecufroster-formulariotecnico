<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormInformacionCabinetTecnica extends Model
{
    protected $table = "form_informacion_cabinet_tecnicas";

    public function form(){
        return $this->belongsTo(FormInformacionFormulario::class,'form_id');
    }
}
