<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = "clients";

    protected $fillable  = [
        'codigo',
        'nombre',
        'calle',
        'fecha_creacion',
        'grupo',
        'telefono',
        'condicion_pago_descripcion',
        'grupo_descripcion',
        'email',
        'territorio',
        'territorio_descripcion',
        'ejecutivo',
        'ejecutivo_telefono',
        'productividad',
        'modelo_atencion',
        'localidad',
        'ciudad',
        'ruc',
    ];
}
