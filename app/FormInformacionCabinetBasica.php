<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormInformacionCabinetBasica extends Model
{
    protected $table = "form_informacion_cabinet_basicas";

    public function form(){
        return $this->belongsTo(FormInformacionFormulario::class,'form_id');
    }
}
