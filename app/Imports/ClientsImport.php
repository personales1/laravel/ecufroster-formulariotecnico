<?php

namespace App\Imports;

use App\Client;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\Importable;

HeadingRowFormatter::default('none');

class ClientsImport implements ToCollection, WithHeadingRow
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $client = Client::where('codigo', $row['CODIGO'])->first();

            if(! $client){
                $client = new Client();
                $client->codigo = $row['CODIGO'] ?? '';
            }

            $client->nombre = $row['Nombre'] ?? '';
            $client->calle = $row['Calle y nº'] ?? '';
            $client->fecha_creacion = $row['Fecha de Creación'] ?? '';
            $client->grupo = $row['Grupo de Clientes'] ?? '';
            $client->telefono = $row['Teléfono 1'] ?? '';
            $client->condicion_pago_descripcion = $row['Descripción Cond.Pago'] ?? '';
            $client->grupo_descripcion = $row['Descr.Gr.Clientes'] ?? '';
            $client->email = $row['E-mail'] ?? '';
            $client->territorio = $row['Territorio'] ?? '';
            $client->territorio_descripcion = $row['Descr.Territorio'] ?? '';
            $client->ejecutivo = $row['ejecutivo'] ?? '';
            $client->ejecutivo_telefono = $row['Tlf ejecutivo'] ?? '';
            $client->productividad = $row['productividad'] ?? '';
            $client->modelo_atencion = $row['modelo de atencion'] ?? '';
            $client->localidad = $row['Localidad'] ?? '';
            $client->ciudad = $row['Ciudad'] ?? '';
            $client->ruc = $row['Ruc'] ?? '';

            $client->save();
        }
    }
}
