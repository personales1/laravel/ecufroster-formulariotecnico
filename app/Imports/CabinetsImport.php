<?php

namespace App\Imports;

use App\Cabinet;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\Importable;

HeadingRowFormatter::default('none');

class CabinetsImport implements ToCollection, WithHeadingRow
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $cabinet = Cabinet::where('marca', $row['Marca'])->where('modelo', $row['Modelos'])->first();

            if(! $cabinet){
                $cabinet = new Cabinet();
                $cabinet->marca = $row['Marca'] ?? '';
                $cabinet->modelo = $row['Modelos'] ?? '';
            }

            $cabinet->tipo = $row['Tipo'] ?? '';
            $cabinet->canastillas_parrillas = $row['Canastillas/Parrillas'] ?? '';

            $cabinet->save();
        }
    }
}
