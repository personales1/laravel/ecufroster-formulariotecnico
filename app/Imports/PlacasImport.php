<?php

namespace App\Imports;

use App\Placa;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\Importable;

HeadingRowFormatter::default('none');

class PlacasImport implements ToCollection, WithHeadingRow
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $placa = Placa::where('placa', $row['PLACA'])->first();

            if(! $placa) {
                $placa = new Placa();
                $placa->placa = $row['PLACA'] ?? '';
            }

            $placa->anio_importacion = $row['ANIO IMPORTACION'] ?? 0;

            $placa->save();
        }
    }
}
