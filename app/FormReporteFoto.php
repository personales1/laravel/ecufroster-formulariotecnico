<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormReporteFoto extends Model
{
    protected $table = "form_reporte_fotos";

    public function form()
    {
        return $this->belongsTo(FormInformacionFormulario::class, 'form_id');
    }
}
