<?php

namespace App\Http\Controllers;

use App\Imports\PlacasImport;
use Illuminate\Http\Request;

class PlacaController extends Controller
{
    public function fileImport(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        (new PlacasImport)->import($request->file('file'));
        return back()->with('exito', 'Los datos de las placas han sido importados exitosamente.');
    }
}
