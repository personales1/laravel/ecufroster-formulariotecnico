<?php

namespace App\Http\Controllers;

use App\FormFirma;
use App\FormFirmasTecnico;
use App\FormInformacionCabinetBasica;
use App\FormInformacionCabinetTecnica;
use App\FormInformacionCliente;
use App\FormInformacionFormulario;
use App\FormObservacionTecnica;
use App\FormReporteFoto;
use App\Log;
use App\Tecnico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiFormulariosController extends Controller
{
    public function guardarFormulario(Request $request){
        try{
            DB::beginTransaction();

            //TABLA form_informacion_formulario
            $formInformacionFormulario = new FormInformacionFormulario();
            $formInformacionFormulario->numero_requerimiento = "";
            $formInformacionFormulario->cliente = $request->form_informacion_formulario['cliente'];
            $formInformacionFormulario->tipo_formulario = "";
            $formInformacionFormulario->tecnico_asignado = $request->form_informacion_formulario['tecnico_asignado'];
            $formInformacionFormulario->fecha_requerimiento = $request->form_informacion_formulario['fecha_requerimiento'];
            $formInformacionFormulario->hora_requerimiento = $request->form_informacion_formulario['hora_requerimiento'];
            $formInformacionFormulario->id_form_app = $request->form_informacion_formulario['id_formulario'];
            $formInformacionFormulario->tecnico_id = $request->id_usuario_app;
            $formInformacionFormulario->fecha_finalizado_app = $request->formulario_app['fecha_finalizado'];
            $formInformacionFormulario->hora_finalizado_app = $request->formulario_app['hora_finalizado'];
            $formInformacionFormulario->id_tipo_formulario = $request->formulario_app['id_tipo_formulario'];
            $formInformacionFormulario->save();

            $formInformacionFormulario->numero_requerimiento = "REQ-TEC-".$formInformacionFormulario->id;
            $formInformacionFormulario->save();

            $idFormulario = $formInformacionFormulario->id;

            //TABLA form_informacion_clientes
            $formInformacionCliente = new FormInformacionCliente();
            $formInformacionCliente->codigo_sap = $request->form_informacion_cliente['codigo_sap'];
            $formInformacionCliente->cliente = $request->form_informacion_cliente['cliente'];
            $formInformacionCliente->local_nombre = $request->form_informacion_cliente['local_nombre'];
            $formInformacionCliente->direccion = $request->form_informacion_cliente['local_direccion'];
            $formInformacionCliente->telefono = $request->form_informacion_cliente['local_telefono'];
            $formInformacionCliente->ciudad = $request->form_informacion_cliente['local_ciudad'];
            $formInformacionCliente->foraneo = $request->form_informacion_cliente['foraneo'];
            $formInformacionCliente->territorio = $request->form_informacion_cliente['territorio'];
            $formInformacionCliente->vendedor = $request->form_informacion_cliente['vendedor'];
            $formInformacionCliente->vendedor_telefono = $request->form_informacion_cliente['vendedor_telefono'];
            $formInformacionCliente->latitud = $request->form_informacion_cliente['latitud'];
            $formInformacionCliente->longitud = $request->form_informacion_cliente['longitud'];
            $formInformacionCliente->form_id = $idFormulario;
            $formInformacionCliente->save();

            //TABLA form_informacion_cabinet_basicas
            $formInformacionCabinetBasica = new FormInformacionCabinetBasica();
            $formInformacionCabinetBasica->marca = $request->form_informacion_basica_cabinet['marca'];
            $formInformacionCabinetBasica->modelo = $request->form_informacion_basica_cabinet['modelo'];
            $formInformacionCabinetBasica->tipo_equipo = $request->form_informacion_basica_cabinet['tipo_equipo'];
            $formInformacionCabinetBasica->numero_canastillas = $request->form_informacion_basica_cabinet['num_canastilas'];
            $formInformacionCabinetBasica->placa = $request->form_informacion_basica_cabinet['placa'];
            $formInformacionCabinetBasica->serie = $request->form_informacion_basica_cabinet['serie'];
            $formInformacionCabinetBasica->anio_importacion = $request->form_informacion_basica_cabinet['anio_importacion'];
            $formInformacionCabinetBasica->form_id = $idFormulario;
            $formInformacionCabinetBasica->save();

            if($request->form_informacion_tecnica_cabinet['id_formulario'] > 0) {
                //TABLA form_informacion_cabinet_tecnicas
                $formInformacionCabinetTecnica = new FormInformacionCabinetTecnica();
                $formInformacionCabinetTecnica->tipo_gas = $request->form_informacion_tecnica_cabinet['tipo_gas'];
                $formInformacionCabinetTecnica->familia_tipo_gas = $request->form_informacion_tecnica_cabinet['familia_tipo_gas'];
                $formInformacionCabinetTecnica->carga_gas = $request->form_informacion_tecnica_cabinet['carga_gas'];
                $formInformacionCabinetTecnica->voltaje = $request->form_informacion_tecnica_cabinet['voltaje'];
                $formInformacionCabinetTecnica->consumo = $request->form_informacion_tecnica_cabinet['consumo'];
                $formInformacionCabinetTecnica->kwh_24_horas = $request->form_informacion_tecnica_cabinet['kwh_24_horas'];
                $formInformacionCabinetTecnica->kwh_mes_aproximado = $request->form_informacion_tecnica_cabinet['kwh_mes_aproximado'];
                $formInformacionCabinetTecnica->beneficios_medio_ambiente = $request->form_informacion_tecnica_cabinet['beneficio_medio_ambiente'];
                $formInformacionCabinetTecnica->form_id = $idFormulario;
                $formInformacionCabinetTecnica->save();
            }


            //TABLA form_observacion_tecnicas
            $formObservacionTecnica = new FormObservacionTecnica();
            $formObservacionTecnica->status_cabinet = $request->form_observacion_tecnica['tipo_status'];
            $formObservacionTecnica->tipo_movimiento = $request->form_observacion_tecnica['tipo_movimiento'] ?: "";
            $formObservacionTecnica->observacion_tecnica = $request->form_observacion_tecnica['observacion'];
            $formObservacionTecnica->form_id = $idFormulario;
            $formObservacionTecnica->save();

            //TABLA form_firmas
            $image = $request->form_firma_cliente['ruta_imagen'];  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = $formInformacionCliente->codigo_sap . '_' . 'form' . $idFormulario . '.png';
            Storage::disk('firmas_clientes')->put($imageName, base64_decode($image));

            $formFirma = new FormFirma();
            $formFirma->imagen = Storage::disk('firmas_clientes')->url($imageName);
            $formFirma->form_id = $idFormulario;
            $formFirma->save();

            //TABLA form_reporte_fotos
            $aux = 1;
            foreach($request->form_fotos_reporte as $item){
                $image = $item['ruta_imagen'];  // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = $formInformacionCliente->codigo_sap . '_form_' . $idFormulario . '_foto_' . $aux . '.png';
                Storage::disk('fotos_reporte')->put($imageName, base64_decode($image));

                $formFoto = new FormReporteFoto();
                $formFoto->imagen = Storage::disk('fotos_reporte')->url($imageName);
                $formFoto->form_id = $idFormulario;
                $formFoto->save();

                $aux += 1;
            }

            DB::commit();

            $noticia = [
                'titulo' => "Éxito",
                'mensaje' => "El formulario se ha guardado de manera exitosa",
            ];
            $data = [
                'estado' => 200,
                'noticia' => $noticia,
                'data' => null
            ];

            return response($data, 200);

        } catch (\Exception $exception) {
            DB::rollback();

            $noticia = [
                'titulo' => "Error",
                'mensaje' => $exception->getMessage().', linea: '.$exception->getLine(),
            ];
            $data = [
                'estado' => 500,
                'noticia' => $noticia,
                'data' => null
            ];

            $log = new Log();
            $log->metodo = "ApiFormulariosController::guardarFormulario";
            $log->parametros = $request;
            $log->descripcion = json_encode($data);
            $log->save();

            return response($data, 500);
        }
    }

    public function guardarFirmaTecnico(Request $request){
        try{
            DB::beginTransaction();

            $tecnico = Tecnico::find($request->id_tecnico);

            //TABLA form_firmas_tecnicos
            $image = $request->firma;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = "firma_tecnico_" . $tecnico->identificacion . '.png';
            Storage::disk('firmas_tecnico')->put($imageName, base64_decode($image));

            $firmaTecnico = FormFirmasTecnico::where('tecnico_id', $request->id_tecnico)->first();

            if(! isset($firmaTecnico)){
                $firmaTecnico = new FormFirmasTecnico();
                $firmaTecnico->tecnico_id = $request->id_tecnico;
            }

            $firmaTecnico->imagen = Storage::disk('firmas_tecnico')->url($imageName);
            $firmaTecnico->save();

            DB::commit();

            $noticia = [
                'titulo' => "Éxito",
                'mensaje' => "La firma del técnico se ha guardado en el servidor de manera exitosa.",
            ];
            $data = [
                'estado' => 200,
                'noticia' => $noticia,
                'data' => null
            ];

            return response($data, 200);
        } catch (\Exception $exception) {
            DB::rollback();

            $noticia = [
                'titulo' => "Error",
                'mensaje' => $exception->getMessage().', linea: '.$exception->getLine(),
            ];
            $data = [
                'estado' => 500,
                'noticia' => $noticia,
                'data' => null
            ];

            $log = new Log();
            $log->metodo = "ApiFormulariosController::guardarFirmaTecnico";
            $log->parametros = $request;
            $log->descripcion = json_encode($data);
            $log->save();

            return response($data, 500);
        }
    }
}
