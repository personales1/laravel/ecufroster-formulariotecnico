<?php

namespace App\Http\Controllers;

use App\Exports\FormularioTecnicoExport;
use App\FormInformacionFormulario;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FormularioTecnicoController extends Controller
{
    public function showByTipoFormulario(int $idTipoFormulario){
        $formularioTecnico = FormInformacionFormulario::where('id_tipo_formulario', $idTipoFormulario)->orderBy('id', 'desc')->paginate(10);
        return view('reporte-form-tecnico-list', ['formularioTecnico'=>$formularioTecnico]);
    }

    /*
    public function show2018(){
        $formularioTecnico = FormInformacionFormulario::where('tipo_reporte', '2018')->orderBy('id', 'desc')->paginate(10);
        return view('reporte-form-tecnico-list', ['formularioTecnico'=>$formularioTecnico]);
    }

    public function show2019(){
        $formularioTecnico = FormInformacionFormulario::where('tipo_reporte', '2019')->orderBy('id', 'desc')->paginate(10);
        return view('reporte-form-tecnico-list', ['formularioTecnico'=>$formularioTecnico]);
    }

    public function show2022_2018(){
        $formularioTecnico = FormInformacionFormulario::where('tipo_reporte', '2022-2018')->orderBy('id', 'desc')->paginate(10);
        return view('reporte-form-tecnico-list', ['formularioTecnico'=>$formularioTecnico]);
    }
    */

    public function exportPdf($idFormulario){
        $result = FormInformacionFormulario::where('id', $idFormulario)->first();
        view()->share('formularioTecnico', $result);

        $pdf = PDF::loadView('pdf-reporte-formulario-tecnico');
        return $pdf->stream('reporte_'. $result->numero_requerimiento .'.pdf');
        //return $pdf->download('reporte_'. $result->numero_requerimiento .'.pdf');
    }

    public function exportExcel($idFormulario){
        $result = FormInformacionFormulario::where('id', $idFormulario)->first();
        return Excel::download(new FormularioTecnicoExport($result), 'formularioTecnico.xls');
    }
}
