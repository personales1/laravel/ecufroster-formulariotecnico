<?php

namespace App\Http\Controllers;

use App\CabinetStatu;
use App\Imports\CabinetsImport;
use Illuminate\Http\Request;

class CabinetController extends Controller
{
    public function showStatus(){
        $status = CabinetStatu::all();
        return view('cabinet-status-list', ['status'=>$status]);
    }

    public function createStatus(){
        return view('cabinet-status-new');
    }

    public function showFormStatus($id){
        $status = CabinetStatu::find($id);

        if($status == null){
            return view('error404');
        }

        return view('cabinet-status-new', compact('status'));
    }

    public function storeStatus(Request $request, $id = null){
        $request->validate([
            'name' => 'required',
        ]);

        if(isset($id)){
            $status = CabinetStatu::find($id);
            $messageSuccess = config('constant.alert.success.update');
        }else{
            $status = new CabinetStatu();
            $messageSuccess = "El técnico ". $request->name . " se ha creado satisfactoriamente.";
        }

        $status->nombre = $request->name;
        $status->save();

        return redirect()->route('cabinet.status.list')->with(['success'=> $messageSuccess]);
    }

    public function fileImport(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        (new CabinetsImport)->import($request->file('file'));
        return back()->with('exito', 'Los datos de los cabinets han sido importados exitosamente.');
    }
}
