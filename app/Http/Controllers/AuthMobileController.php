<?php

namespace App\Http\Controllers;

use App\Tecnico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthMobileController extends Controller
{
    public function login(Request $request){
        $tecnico = Tecnico::where('identificacion', $request->usuario)->first();

        if($tecnico){
            if(Hash::check($request->password, $tecnico->password)){
                return 1;
            }
            return 0;
        }
        return 0;
    }

    public function perfil($identificacion){
        $tecnico = Tecnico::where('identificacion', $identificacion)->first();
        return $tecnico;
    }
}
