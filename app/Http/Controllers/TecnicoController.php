<?php

namespace App\Http\Controllers;

use App\Tecnico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TecnicoController extends Controller
{
    public function show(){
        $tecnicos = Tecnico::all();
        return view('tecnico-list', ['tecnicos'=>$tecnicos]);
    }

    public function create(){
        return view('tecnico-new');
    }

    public function showForm($id){
        $tecnico = Tecnico::find($id);

        if($tecnico == null){
            return view('error404');
        }

        return view('tecnico-new', compact('tecnico'));
    }

    public function store(Request $request, $id = null){
        $request->validate([
            'name' => 'required',
            'identificacion' => 'required',
            'password' => 'required',
        ]);

        if(isset($id)){
            $tecnico = Tecnico::find($id);
            $messageSuccess = config('constant.alert.success.update');
        }else{
            $tecnico = new Tecnico();
            $messageSuccess = "El técnico ". $request->name . " se ha creado satisfactoriamente.";
        }

        $tecnico->nombre = $request->name;
        $tecnico->identificacion = $request->identificacion;
        $tecnico->telefono = $request->telefono;
        $tecnico->correo = $request->correo;
        $tecnico->password = Hash::make($request->password);
        $tecnico->save();

        return redirect()->route('tecnico.list')->with(['success'=> $messageSuccess]);
    }
}
