<?php

namespace App\Http\Controllers;

use App\Imports\ClientsImport;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function fileImport(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        //(new ClientsImport)->import($request->file('file')->store('temp')); con la función store, se crea el archivo excel dentro de storage/app
        (new ClientsImport)->import($request->file('file'));
        return back()->with('exito', 'Los datos de los clientes han sido importados exitosamente.');
    }
}
