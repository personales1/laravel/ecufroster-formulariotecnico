<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFirma extends Model
{
    protected $table = "form_firmas";

    public function form(){
        return $this->belongsTo(FormInformacionFormulario::class,'form_id');
    }
}
