<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormInformacionCliente extends Model
{
    protected $table = "form_informacion_clientes";

    public function form(){
        return $this->belongsTo(FormInformacionFormulario::class,'form_id');
    }
}
