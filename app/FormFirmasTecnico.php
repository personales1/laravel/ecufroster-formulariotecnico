<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFirmasTecnico extends Model
{
    protected $table = "form_firmas_tecnicos";

    public function tecnico(){
        return $this->belongsTo(Tecnico::class);
    }
}
