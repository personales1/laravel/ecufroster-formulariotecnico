<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabinet extends Model
{
    protected $table = "cabinets";

    protected $fillable  = [
        'marca',
        'modelo',
        'tipo',
        'canastillas_parrillas',
    ];
}
