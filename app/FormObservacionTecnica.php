<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormObservacionTecnica extends Model
{
    protected $table = "form_observacion_tecnicas";

    public function form(){
        return $this->belongsTo(FormInformacionFormulario::class,'form_id');
    }
}
