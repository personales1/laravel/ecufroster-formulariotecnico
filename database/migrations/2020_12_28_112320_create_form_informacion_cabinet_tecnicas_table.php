<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInformacionCabinetTecnicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_informacion_cabinet_tecnicas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('form_id');
            $table->text('tipo_gas');
            $table->text('familia_tipo_gas');
            $table->text('carga_gas');
            $table->text('voltaje');
            $table->text('consumo');
            $table->text('kwh_24_horas')->nullable();
            $table->text('kwh_mes_aproximado')->nullable();
            $table->text('beneficios_medio_ambiente')->nullable();
            $table->timestamps();

            $table->foreign('form_id')->references('id')->on('form_informacion_formularios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_informacion_cabinet_tecnicas');
    }
}
