<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInformacionClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_informacion_clientes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('form_id');
            $table->text('codigo_sap');
            $table->text('cliente');
            $table->text('local_nombre');
            $table->text('direccion');
            $table->text('telefono');
            $table->text('ciudad');
            $table->text('foraneo');
            $table->text('territorio');
            $table->text('vendedor');
            $table->text('vendedor_telefono');
            $table->text('latitud')->nullable();
            $table->text('longitud')->nullable();
            $table->timestamps();

            $table->foreign('form_id')->references('id')->on('form_informacion_formularios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_informacion_clientes');
    }
}
