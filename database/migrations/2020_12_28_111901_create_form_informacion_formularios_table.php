<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInformacionFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_informacion_formularios', function (Blueprint $table) {
            $table->id();
            $table->text('numero_requerimiento');
            $table->text('cliente');
            $table->text('tipo_formulario');
            $table->text('tecnico_asignado');
            $table->text('fecha_requerimiento');
            $table->text('hora_requerimiento');
            $table->text('fecha_finalizado_app');
            $table->text('hora_finalizado_app');
            $table->text('id_form_app');
            $table->unsignedBigInteger('tecnico_id');
            $table->timestamps();

            $table->foreign('tecnico_id')->references('id')->on('tecnicos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_informacion_formularios');
    }
}
