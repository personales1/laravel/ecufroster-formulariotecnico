<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInformacionCabinetBasicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_informacion_cabinet_basicas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('form_id');
            $table->text('marca');
            $table->text('modelo');
            $table->text('tipo_equipo');
            $table->text('numero_canastillas');
            $table->text('placa');
            $table->text('serie');
            $table->timestamps();

            $table->foreign('form_id')->references('id')->on('form_informacion_formularios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_informacion_cabinet_basicas');
    }
}
