<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTipoMovimientoToFormObservacionTecnicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_observacion_tecnicas', function (Blueprint $table) {
            $table->text('tipo_movimiento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_observacion_tecnicas', function (Blueprint $table) {
            $table->dropColumn('tipo_movimiento');
        });
    }
}
