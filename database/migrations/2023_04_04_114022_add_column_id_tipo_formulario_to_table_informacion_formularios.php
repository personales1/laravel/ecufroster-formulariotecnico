<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIdTipoFormularioToTableInformacionFormularios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_informacion_formularios', function (Blueprint $table) {
            $table->bigInteger('id_tipo_formulario')->unsigned()->nullable();

            $table->foreign('id_tipo_formulario', 'inf_form_id_tipo_form_foregin')->references('id')->on('formularios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_informacion_formularios', function (Blueprint $table) {
            $table->dropColumn('id_tipo_formulario');
        });
    }
}
