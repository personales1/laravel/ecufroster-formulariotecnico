<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('codigo')->unique();
            $table->string('nombre');
            $table->string('calle');
            $table->string('fecha_creacion');
            $table->string('grupo');
            $table->string('telefono');
            $table->string('condicion_pago_descripcion');
            $table->string('grupo_descripcion');
            $table->string('email');
            $table->string('territorio');
            $table->string('territorio_descripcion');
            $table->string('ejecutivo');
            $table->string('ejecutivo_telefono');
            $table->string('productividad');
            $table->string('modelo_atencion');
            $table->string('localidad');
            $table->string('ciudad');
            $table->string('ruc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
