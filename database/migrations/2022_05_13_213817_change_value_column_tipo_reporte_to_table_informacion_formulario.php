<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeValueColumnTipoReporteToTableInformacionFormulario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_informacion_formularios', function (Blueprint $table) {
            $table->string('tipo_reporte')->default('2022-2018')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_informacion_formularios', function (Blueprint $table) {
            $table->string('tipo_reporte')->default('2019')->change();
        });
    }
}
