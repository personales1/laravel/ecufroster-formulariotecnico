<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_profiles')->insert([
            'user_id' => 1,
            'nombres' => 'Admin',
            'apellidos' => 'Sistema',
            'identificacion' => '0999999999',
            'telefono' => '0999999999',
        ]);
    }
}
