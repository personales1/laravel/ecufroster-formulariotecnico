<?php

use Illuminate\Database\Seeder;

class Formulario4Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formularios')->insert([
            'nombre' => 'Importación 2019',
            'codigo' => 'IMP_2019',
        ]);
    }
}
