<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'rol_id' => 'ADMIN',
            'nombre' => 'Administrador',
        ]);

        DB::table('roles')->insert([
            'rol_id' => 'VENTA',
            'nombre' => 'Ventas',
        ]);
    }
}
