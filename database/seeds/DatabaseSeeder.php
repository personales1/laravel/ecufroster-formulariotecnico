<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            UsersProfileTableSeeder::class,
            FormularioSeeder::class,
            CabinetStatusSeeder::class,
            Formulario2Seeder::class,
            Formulario3Seeder::class,
            Formulario4Seeder::class,
        ]);
    }
}
