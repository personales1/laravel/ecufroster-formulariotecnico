<?php

use Illuminate\Database\Seeder;

class Formulario3Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formularios')->insert([
            'nombre' => 'Importación 2013-2018',
            'codigo' => 'IMP_2013_2018',
        ]);
    }
}