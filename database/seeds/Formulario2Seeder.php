<?php

use Illuminate\Database\Seeder;

class Formulario2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formularios')->insert([
            'nombre' => 'Movilización de cabinets',
            'codigo' => 'MOV_CABINETS',
        ]);
    }
}
