<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CabinetStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cabinet_status')->insert([
            'nombre' => 'Operativo',
        ]);
        DB::table('cabinet_status')->insert([
            'nombre' => 'Visita fallida',
        ]);
        DB::table('cabinet_status')->insert([
            'nombre' => 'Orden de retiro',
        ]);
    }
}
