<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormularioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formularios')->insert([
            'nombre' => 'Medición de consumo KWH en PDV',
            'codigo' => 'CONSUMO_KWH_PDV',
        ]);

        DB::table('formularios')->insert([
            'nombre' => 'Emergencia en Ciudad',
            'codigo' => 'EMERGENCIA_CIUDAD',
        ]);
    }
}
